#!/bin/bash

# Configurations
GITLAB_URL="https://gitlab.com"  # Changez cette URL si vous utilisez une instance GitLab auto-hébergée.
GITLAB_BADGE="![project](https://img.shields.io/badge/Project-2ECE53?style=for-the-badge&logo=gitlab&logoColor=white)"
DOCUMENTATION_BADGE="![documentation](https://img.shields.io/badge/Documentation-2ECE53?style=for-the-badge&logo=googledocs&logoColor=white)"
# Fonction pour convertir snake_case en Title Case
to_title_case() {
    echo "$1" | sed -r 's/-/ /g' | sed -r 's/\<./\U&/g'
}

# Récupérer la liste des projets du groupe spécifié
projects=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_URL/api/v4/groups/$GROUP_ID/projects?include_subgroups=true&per_page=100")

# Parcourir chaque projet
echo "$projects" | jq -r '.[] | select(.readme_url != null) | .id, .name, .web_url, .readme_url' | while read -r id; read -r name; read -r web_url; read -r readme_url; do
  # Créer un nom de fichier sûr pour le fichier README
  safe_name=$(echo "$name" | tr '[:upper:]' '[:lower:]' | tr ' ' '_')
  
  # Télécharger le fichier README.md
  readme_raw_url=$(echo "$readme_url" | sed 's/-\/blob/-\/raw/')
  curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$readme_raw_url" -o "./docs/${safe_name}.md" --fail

  # Vérifier si le téléchargement a réussi
  if [ $? -eq 0 ]; then
    title=$(to_title_case "$name")
    badge="[$GITLAB_BADGE]($web_url)"
    doc_badge="[$DOCUMENTATION_BADGE](https://vvoyerlab.gitlab.io/$name)"
    # Ajouter la métadonnée title pour mkdocs
    echo "---\ntitle: $title\n---\n$badge $doc_badge  \n$(cat "./docs/${safe_name}.md")" > "./docs/${safe_name}.md"
    echo "Téléchargé et modifié: ${safe_name}.md"
  else
    echo "Échec du téléchargement de README.md pour le projet: $name"
  fi
done
